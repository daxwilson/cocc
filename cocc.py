#!/usr/bin/env python

''' Clash of Clans Future Value Calculator

A simple future value calculator for Clash of Clans gold mines and
elixir collectors.

Enter the current gold and/or elixir amount, the desired amount needed,
and the calculator will display a date and time of when
it will be completed.

For example, if your current elixir amount is 123456 and you need 750000
to train a wizard, then it will calculate how long it will take to reach
your goal at your current production rate.

cocc -e 123456 750000

Calculator prints the following (i.e. current time: August 5th 9:36 PM).

Elixir Production

    Rate: 300 per minute (Boosted: 600)
    Goal: 626544 (123456 of 750000)
    Time: 1d 10h 48m => Aug 07 08:23 AM (Boosted: 17h 24m => Aug 06 02:59 PM)

The calculated time is based on your current elixir production rate.
Elixir and gold production rates are stored in a configuration file.
Edit the configuration through the calcuator interface.

For example, if you have six level 11 elixir collectors, then run
the following command.

cocc -ae 11 6

Here are all the commands.

Display help.

cocc -h or --help

Calculate elixir production.

cocc -e [current] [desired]

Calculate gold production.

cocc -g [current] [desired]

Display current configuration.

cocc -c or --cfg

Add or remove elixir collectors to the configuration.

cocc -ae [level] [count] # add elixir collectors
cocc -re [level] [count] # remove elixir collectors

Add or remove gold mines to the configuration.

cocc -ag [level] [count] # add gold mines
cocc -rg [level] [count] # remove gold mines

Or do it all at once.

cocc -ae 11 6 --ag 11 6 --cfg -e 123456 750000 -g 654321 800000

'''

import argparse
import datetime
import json
import os
import sys

__author__ = 'Dax Wilson'
__copyright__ = 'Copyright 2014. All rights reserved.'
__credits__ = ['Dax Wilson']

__license__ = 'GNU-GPL'
__version__ = '1.0'
__maintainer__ = 'Dax Wilson'
__email__ = 'daxwilson@gmail.com'
__status__ = 'Production'

EMPTY_CFG = {'gold':{}, 'elixir':{}}
CFG_FILE = 'cocc.cfg'
TIME_FORMAT = "%b %d %I:%M %p"

# elixir collector production rates by level
elixir = {1: 200.0, 2: 400.0, 3: 600.0, 4: 800.0, 5: 1000.0, 6: 1300.0, 7: 1600.0, 8: 1900.0, 9: 2200.0, 10: 2500.0, 11: 3000.0}

# gold mine production rates by level
gold = {1: 200.0, 2: 400.0, 3: 600.0, 4: 800.0, 5: 1000.0, 6: 1300.0, 7: 1600.0, 8: 1900.0, 9: 2200.0, 10: 2500.0, 11: 3000.0}

def main(argparser):

    """ The main calculation method. Arguments are pulled from argparse. """

    # parse the arguments
    args = argparser.parse_args()

    # load the configuration file
    cfg = json.load(open(CFG_FILE))

    # edit the configuration if ae, ag, re, rg flags are present
    cfg_changed = False

    if args.ae:
        _add_cfg(cfg['elixir'], elixir.keys(), args.ae[0], args.ae[1])
        cfg_changed = True

    if args.ag:
        _add_cfg(cfg['gold'], gold.keys(), args.ag[0], args.ag[1])
        cfg_changed = True

    if args.re:
        _rem_cfg(cfg['elixir'], elixir.keys(), args.re[0], args.re[1])
        cfg_changed = True

    if args.rg:
        _rem_cfg(cfg['gold'], gold.keys(), args.rg[0], args.rg[1])
        cfg_changed = True

    if cfg_changed:
        json.dump(cfg, open(CFG_FILE, mode='w'))

    # display current configuration if cfg flag is present
    if args.cfg:
        print('Elixir Collector Configuration')
        if len(cfg['elixir']) == 0:
            print("\tNone")
        for level, count in cfg['elixir'].items():
            print('\tLevel {0}: {1}'.format(level, count))
        print('')
        print('Gold Mine Configuration')
        if len(cfg['gold']) == 0:
            print("\tNone")
        for level, count in cfg['gold'].items():
            print('\tLevel {0}: {1}'.format(level, count))
        print('')

    rpm = 0.0

    # calculate elixir future value
    if args.e:
        if args.e[0] > args.e[1]:
            print('Error: Current elixir amount cannot be greater than desired amount.')
            return None

        rpm = 0.0
        if len(cfg['elixir']) > 0:
            for level, count in cfg['elixir'].items():
                rpm += ((elixir[int(level)] / 60.0) * count)

            _calc_time('Elixir', rpm, args.e[0], args.e[1])
        else:
            print('Add to your configuration before calculations can be made.\n')
            print("Add elixir collectors - 'cocc -ae level count'")
            print('\te.g. cocc -ae 11 1 - Adds one level 11 elixir collector')
            print('')

    # calculate gold future value
    if args.g:
        if args.g[0] > args.g[1]:
            print('Error: Current gold amount cannot be greater than desired amount.')
            return None

        rpm = 0.0
        if len(cfg['gold']) > 0:
            for level, count in cfg['gold'].items():
                rpm += ((gold[int(level)] / 60.0) * count)

            _calc_time('Gold', rpm, args.g[0], args.g[1])
        else:
            print('Add to your configuration before calculations can be made.\n')
            print("Add gold mines - 'cocc -ag level count'")
            print('\te.g. cocc -ag 11 6 - Adds six level 11 gold mines')
            print('')

def _calc_time(name, rpm, current, desired):

    """ Calculates the time needed to reach the desired amount. """

    diff = desired - current
    mins = diff / rpm
    mins2x = diff / (rpm * 2.0)
    future = datetime.datetime.now() + datetime.timedelta(minutes = int(mins))
    future2x = datetime.datetime.now() + datetime.timedelta(minutes = int(mins2x))

    print('{0} Production'.format(name))
    print("\tRate:\t\t{0} per minute (Boosted: {1})".format(int(rpm), int(rpm * 2.0)))
    print('\tGoal:\t\t{2} ({0} of {1})'.format(int(current), int(desired), int(diff)))
    print('\tTime: {2} => {0} (Boosted: {3} => {1})'.format(future.strftime(TIME_FORMAT)
                                                    , future2x.strftime(TIME_FORMAT)
                                                    , _mins_to_coctime(mins)
                                                    , _mins_to_coctime(mins2x)))

    print('')

def _add_cfg(cfg_dict, levels, level, count):
    
    """ Adds level and count to one of the configuration dictionaries. """

    if level in levels:
        lvl = str(level)

        if lvl in cfg_dict:
            cfg_dict[lvl] += count
        else:
            cfg_dict[lvl] = count

def _rem_cfg(cfg_dict, levels, level, count):

    """ Removes level and count from one of the configuration dictionaries. """

    if level in levels:
        lvl = str(level)

        if lvl in cfg_dict:
            cfg_dict[lvl] -= count
            if cfg_dict[lvl] < 0:
                del(cfg_dict[lvl])

def _mins_to_coctime(mins):

    """ Converts total mins to Clash of Clans time format: 1d 2h 3m. """

    result = ''

    hour = 60
    day = 24 * hour

    d = h = m = 0

    d = int(mins / day)
    h = int((mins - (d * day)) / hour)
    m = int(mins - ((d * day) + (h * hour)))

    if d > 0:
        if h > 0:
            if m > 0:
                result = '{0}d {1}h {2}m'.format(d, h, m)
            else:
                result = '{0}d {1}h'.format(d, h)
        else:
            result = '{0}d {1}m'.format(d, m)
    elif h > 0:
        if m > 0:
            result = '{0}h {1}m'.format(h, m)
        else:
            result = '{0}h'.format(h)
    else:
        result = '{0}m'.format(m)

    return result

if __name__ == "__main__":

    print('Clash of Clans Future Value Calculator {0}\n'.format(__version__))

    if not os.path.exists(CFG_FILE):
        json.dump(EMPTY_CFG, open(CFG_FILE, mode='w'))
        
    parser = argparse.ArgumentParser()
    parser.add_argument('-c', '--cfg', action='store_true', help='displays elixir and gold configuration')
    parser.add_argument('-e', nargs=2, type=int, metavar=('current', 'desired'), help='calculates elixir production')
    parser.add_argument('-g', nargs=2, type=int, metavar=('current', 'desired'), help='calculates gold production')
    parser.add_argument('-ae', nargs=2, metavar=('level', 'count'), type=int, help='adds elixir collectors to the configuration')
    parser.add_argument('-re', nargs=2, metavar=('level', 'count'), type=int, help='removes elixir collectors from the configuration')
    parser.add_argument('-ag', nargs=2, metavar=('level', 'count'), type=int, help='adds gold mines to the configuration')
    parser.add_argument('-rg', nargs=2, metavar=('level', 'count'), type=int, help='removes gold mines from the configuration')
    #parser.add_argument('-s', help='time left on shield (e.g. 5d 4h, or 4h 3m, or 3m)')

    main(parser)
