#!/usr/bin/env python

from distutils.core import setup

import cocc

setup(name='cocc',
      version=cocc.__version__,
      description='Clash of Clans Future Value Calculator',
      author=cocc.__author__,
      author_email=cocc.__email__,
      url='https://bitbucket.org/daxwilson/cocc',
      packages=['cocc'],
      long_description=open('README').read(),
      license=propdb.__license__,
     )